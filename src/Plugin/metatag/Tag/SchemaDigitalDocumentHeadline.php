<?php

declare(strict_types = 1);

namespace Drupal\schema_digital_document\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'headline' meta tag.
 *
 * - 'id' should be a globally unique id.
 * - 'name' should match the Schema.org element name.
 * - 'group' should match the id of the group that defines the Schema.org type.
 *
 * @MetatagTag(
 *   id = "schema_digital_document_headline",
 *   label = @Translation("headline"),
 *   description = @Translation("REQUIRED BY GOOGLE. Headline of the digital document."),
 *   name = "headline",
 *   group = "schema_digital_document",
 *   weight = 0,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 *   property_type = "text",
 *   tree_parent = {},
 *   tree_depth = -1,
 * )
 */
class SchemaDigitalDocumentHeadline extends SchemaNameBase {

}
