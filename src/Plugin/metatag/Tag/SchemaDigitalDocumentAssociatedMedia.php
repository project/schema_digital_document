<?php

declare(strict_types = 1);

namespace Drupal\schema_digital_document\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'schema_digital_document_associated_media' meta tag.
 *
 * - 'id' should be a globally unique id.
 * - 'name' should match the Schema.org element name.
 * - 'group' should match the id of the group that defines the Schema.org type.
 *
 * @MetatagTag(
 *   id = "schema_digital_document_associated_media",
 *   label = @Translation("associatedMedia"),
 *   description = @Translation("A media object that encodes this CreativeWork. This property is a synonym for encoding."),
 *   name = "associatedMedia",
 *   group = "schema_digital_document",
 *   weight = 1,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 *   property_type = "media_object",
 *   tree_parent = {
 *     "MediaObject",
 *   },
 *   tree_depth = -1,
 * )
 */
class SchemaDigitalDocumentAssociatedMedia extends SchemaNameBase {

}
