<?php

declare(strict_types = 1);

namespace Drupal\schema_digital_document\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'name' meta tag.
 *
 * - 'id' should be a globally unique id.
 * - 'name' should match the Schema.org element name.
 * - 'group' should match the id of the group that defines the Schema.org type.
 *
 * @MetatagTag(
 *   id = "schema_digital_document_name",
 *   label = @Translation("name"),
 *   description = @Translation("Name (usually the headline of the digital document)."),
 *   name = "name",
 *   group = "schema_digital_document",
 *   weight = 0,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 *   property_type = "text",
 *   tree_parent = {},
 *   tree_depth = -1,
 * )
 */
class SchemaDigitalDocumentName extends SchemaNameBase {

}
