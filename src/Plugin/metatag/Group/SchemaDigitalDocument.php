<?php

declare(strict_types = 1);

namespace Drupal\schema_digital_document\Plugin\metatag\Group;

use Drupal\schema_metatag\Plugin\metatag\Group\SchemaGroupBase;

/**
 * Provides a plugin for the 'DigitalDocument' meta tag group.
 *
 * @MetatagGroup(
 *   id = "schema_digital_document",
 *   label = @Translation("Schema.org: DigitalDocument"),
 *   description = @Translation("See Schema.org definitions for this Schema type at <a href="":url"">:url</a>.", arguments = {
 *     ":url" = "https://schema.org/DigitalDocument",
 *   }),
 *   weight = 10,
 * )
 */
class SchemaDigitalDocument extends SchemaGroupBase {

}
