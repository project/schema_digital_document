<?php

declare(strict_types = 1);

namespace Drupal\schema_digital_document\Plugin\schema_metatag\PropertyType;

use Drupal\schema_metatag\Plugin\schema_metatag\PropertyTypeBase;

/**
 * Provides a plugin for the 'MediaObject' Schema.org property type.
 *
 * @SchemaPropertyType(
 *   id = "media_object",
 *   label = @Translation("MediaObject"),
 *   tree_parent = {
 *     "MediaObject",
 *   },
 *   tree_depth = 2,
 *   property_type = "MediaObject",
 *   sub_properties = {
 *     "@type" = {
 *       "id" = "type",
 *       "label" = @Translation("@type"),
 *       "description" = "",
 *     },
 *     "@id" = {
 *       "id" = "text",
 *       "label" = @Translation("@id"),
 *       "description" = @Translation("Globally unique @id of the media object, usually a url, used to to link other properties to this object."),
 *     },
 *     "name" = {
 *       "id" = "text",
 *       "label" = @Translation("name"),
 *       "description" = @Translation("The name of the work."),
 *     },
 *     "url" = {
 *       "id" = "url",
 *       "label" = @Translation("url"),
 *       "description" = @Translation("Absolute URL of the canonical Web page for the work."),
 *     },
 *     "sameAs" = {
 *       "id" = "url",
 *       "label" = @Translation("sameAs"),
 *       "description" = @Translation("Urls and social media links, comma-separated list of absolute URLs."),
 *     },
 *     "datePublished" = {
 *       "id" = "date",
 *       "label" = @Translation("datePublished"),
 *       "description" = @Translation("Publication date."),
 *     },
 *     "duration" = {
 *       "id" = "duration",
 *       "label" = @Translation("duration"),
 *       "description" = @Translation("The duration of the item in ISO 8601 format."),
 *     },
 *   },
 * )
 */
class MediaObject extends PropertyTypeBase {

}
