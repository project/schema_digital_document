<?php

declare(strict_types = 1);

namespace Drupal\Tests\schema_digital_document\Functional;

use Drupal\Tests\schema_metatag\Functional\SchemaMetatagTagsTestBase;

/**
 * Tests that each of the Schema Metatag Digital Document tags work correctly.
 *
 * @group schema_metatag
 * @group schema_digital_document
 */
class SchemaDigitalDocumentTest extends SchemaMetatagTagsTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['schema_digital_document'];

  /**
   * {@inheritdoc}
   */
  public $moduleName = 'schema_digital_document';

  /**
   * {@inheritdoc}
   */
  public $groupName = 'schema_digital_document';

}
